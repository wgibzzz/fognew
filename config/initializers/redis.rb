if ENV["REDISCLOUD_URL"]
  $redis_online = Redis.new(:url => ENV["REDISCLOUD_URL"], :driver => :hiredis)
else
  $redis_online = Redis.new host: '127.0.0.1', port: '6379', db: 1, driver: :hiredis
end
