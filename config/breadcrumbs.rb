crumb :root do
  link t('fog.fe.layout.nav.home'), root_path
end

crumb :categories do
  link t('fog.fe.layout.nav.categories'), categories_path
end

crumb :category do |category|
  link category.title, category_path(category)
  if category.parent
    parent category.parent
  else
    parent :categories
  end

end

crumb :posts do
  link t('fog.fe.layout.nav.posts'), posts_path
end


crumb :post do |post|
  link post.title, category_post_path(post.category, post)
  parent post.category
end

crumb :forum do
  link I18n.t('fog.fe.layout.nav.forum'), forum_categories_path
end

crumb :forum_category do |category|
  link category.title, forum_category_path(category)
  if category.parent
    parent :forum_category, category.parent
  else
    parent :forum
  end
end

crumb :forum_topic do |topic|
  if topic.title.nil?
    link 'New topic', '#'
  else
    link topic.title, forum_category_topic_path(topic.category, topic)
  end
  parent :forum_category, topic.category
end


# crumb :projects do
#   link "Projects", projects_path
# end

# crumb :project do |project|
#   link project.name, project_path(project)
#   parent :projects
# end

# crumb :project_issues do |project|
#   link "Issues", project_issues_path(project)
#   parent :project, project
# end

# crumb :issue do |issue|
#   link issue.title, issue_path(issue)
#   parent :project_issues, issue.project
# end

# If you want to split your breadcrumbs configuration over multiple files, you
# can create a folder named `config/breadcrumbs` and put your configuration
# files there. All *.rb files (e.g. `frontend.rb` or `products.rb`) in that
# folder are loaded and reloaded automatically when you change them, just like
# this file (`config/breadcrumbs.rb`).