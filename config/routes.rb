# require 'sidekiq/web'
Rails.application.routes.draw do
  get 'errors/show'

  root :to => 'posts#index'
  # mount Sidekiq::Web => '/sidekiq'

  scope '/api' do
    resources :comments, only: [:create, :update, :destroy]
    resources :replies, only: [:create, :update, :destroy]
    namespace :forum do
      resources :topics, only: [:create, :update, :destroy]
    end
  end
  resources :posts

  resources :categories do
    resources :posts
  end

  get '/forum', to: 'forum/categories#index'
  namespace :forum do
    resources :categories, only: [:index, :show, :create, :new, :update] do
      resources :topics, only: [:index, :show, :create, :new, :update]  do
        resources :replies, only: [:index, :show, :create, :new, :update]
      end
    end
  end
  resources :users
  devise_for :users, :path => '/auth'
  scope '/json' do
    post '/settings', :to => 'application#set_forum_collapse', :as => 'forum_settings'
    resources :users, only: [:index, :show] do
      resources :posts, only: [:index, :show]
      resources :comments, only: [:index, :show]
    end
  end
  get '/categories/:id', to: 'categories#show', :as => 'blog_category'
  get '/categories/:category_id/:id', to: 'posts#show', :as => 'blog_post'
  get '/forums', to: 'forum/categories#index', :as => 'forums_index'
  get '/forums/:id', to: 'forum/categories#show', :as => 'forums_category'
  get '/forums/:category_id/:id', to: 'forum/topics#show', :as => 'forums_topic'
end
