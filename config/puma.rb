environment 'production'
bind 'unix:///home/fog/testing/shared/tmp/sockets/puma.sock'
state_path '/home/fog/testing/shared/tmp/sockets/puma.state'
activate_control_app 'unix:///home/fog/testing/shared/tmp/sockets/pumactl.sock'
pidfile '/home/fog/testing/shared/tmp/pids/puma.pid'
stdout_redirect '/home/fog/testing/shared/log/puma.status.log', '/home/fog/testing/shared/log/puma.error.log'
daemonize true
threads 8,32
workers 4
preload_app!
on_worker_boot do
  ActiveSupport.on_load(:active_record) do
    ActiveRecord::Base.establish_connection
  end
end