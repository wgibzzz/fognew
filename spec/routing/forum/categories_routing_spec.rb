require "rails_helper"

RSpec.describe Forum::CategoriesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/forum/categories").to route_to("forum/categories#index")
    end

    it "routes to #new" do
      expect(:get => "/forum/categories/new").to route_to("forum/categories#new")
    end

    it "routes to #show" do
      expect(:get => "/forum/categories/1").to route_to("forum/categories#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/forum/categories/1/edit").to route_to("forum/categories#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/forum/categories").to route_to("forum/categories#create")
    end

    it "routes to #update" do
      expect(:put => "/forum/categories/1").to route_to("forum/categories#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/forum/categories/1").to route_to("forum/categories#destroy", :id => "1")
    end

  end
end
