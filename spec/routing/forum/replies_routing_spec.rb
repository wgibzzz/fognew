require "rails_helper"

RSpec.describe Forum::RepliesController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/forum/replies").to route_to("forum/replies#index")
    end

    it "routes to #new" do
      expect(:get => "/forum/replies/new").to route_to("forum/replies#new")
    end

    it "routes to #show" do
      expect(:get => "/forum/replies/1").to route_to("forum/replies#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/forum/replies/1/edit").to route_to("forum/replies#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/forum/replies").to route_to("forum/replies#create")
    end

    it "routes to #update" do
      expect(:put => "/forum/replies/1").to route_to("forum/replies#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/forum/replies/1").to route_to("forum/replies#destroy", :id => "1")
    end

  end
end
