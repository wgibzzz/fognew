require "rails_helper"

RSpec.describe Forum::TopicsController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/forum/topics").to route_to("forum/topics#index")
    end

    it "routes to #new" do
      expect(:get => "/forum/topics/new").to route_to("forum/topics#new")
    end

    it "routes to #show" do
      expect(:get => "/forum/topics/1").to route_to("forum/topics#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/forum/topics/1/edit").to route_to("forum/topics#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/forum/topics").to route_to("forum/topics#create")
    end

    it "routes to #update" do
      expect(:put => "/forum/topics/1").to route_to("forum/topics#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/forum/topics/1").to route_to("forum/topics#destroy", :id => "1")
    end

  end
end
