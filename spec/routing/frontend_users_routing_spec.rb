require "rails_helper"

RSpec.describe FrontendUsersController, :type => :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/frontend_users").to route_to("frontend_users#index")
    end

    it "routes to #new" do
      expect(:get => "/frontend_users/new").to route_to("frontend_users#new")
    end

    it "routes to #show" do
      expect(:get => "/frontend_users/1").to route_to("frontend_users#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/frontend_users/1/edit").to route_to("frontend_users#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/frontend_users").to route_to("frontend_users#create")
    end

    it "routes to #update" do
      expect(:put => "/frontend_users/1").to route_to("frontend_users#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/frontend_users/1").to route_to("frontend_users#destroy", :id => "1")
    end

  end
end
