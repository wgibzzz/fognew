require 'rails_helper'

RSpec.describe "categories/edit", :type => :view do
  before(:each) do
    @category = assign(:category, Category.create!(
      :title => "MyString",
      :desc => "MyText",
      :parent_id => 1,
      :user_id => 1
    ))
  end

  it "renders the edit category form" do
    render

    assert_select "form[action=?][method=?]", category_path(@category), "post" do

      assert_select "input#category_title[name=?]", "category[title]"

      assert_select "textarea#category_desc[name=?]", "category[desc]"

      assert_select "input#category_parent_id[name=?]", "category[parent_id]"

      assert_select "input#category_user_id[name=?]", "category[user_id]"
    end
  end
end
