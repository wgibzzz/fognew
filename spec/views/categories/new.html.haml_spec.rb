require 'rails_helper'

RSpec.describe "categories/new", :type => :view do
  before(:each) do
    assign(:category, Category.new(
      :title => "MyString",
      :desc => "MyText",
      :parent_id => 1,
      :user_id => 1
    ))
  end

  it "renders new category form" do
    render

    assert_select "form[action=?][method=?]", categories_path, "post" do

      assert_select "input#category_title[name=?]", "category[title]"

      assert_select "textarea#category_desc[name=?]", "category[desc]"

      assert_select "input#category_parent_id[name=?]", "category[parent_id]"

      assert_select "input#category_user_id[name=?]", "category[user_id]"
    end
  end
end
