require 'rails_helper'

RSpec.describe "forum/topics/show", :type => :view do
  before(:each) do
    @forum_topic = assign(:forum_topic, Forum::Topic.create!(
      :title => "Title",
      :desc => "MyText",
      :is_published => false,
      :user_id => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/1/)
  end
end
