require 'rails_helper'

RSpec.describe "forum/topics/new", :type => :view do
  before(:each) do
    assign(:forum_topic, Forum::Topic.new(
      :title => "MyString",
      :desc => "MyText",
      :is_published => false,
      :user_id => 1
    ))
  end

  it "renders new forum_topic form" do
    render

    assert_select "form[action=?][method=?]", forum_topics_path, "post" do

      assert_select "input#forum_topic_title[name=?]", "forum_topic[title]"

      assert_select "textarea#forum_topic_desc[name=?]", "forum_topic[desc]"

      assert_select "input#forum_topic_is_published[name=?]", "forum_topic[is_published]"

      assert_select "input#forum_topic_user_id[name=?]", "forum_topic[user_id]"
    end
  end
end
