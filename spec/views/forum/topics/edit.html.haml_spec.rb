require 'rails_helper'

RSpec.describe "forum/topics/edit", :type => :view do
  before(:each) do
    @forum_topic = assign(:forum_topic, Forum::Topic.create!(
      :title => "MyString",
      :desc => "MyText",
      :is_published => false,
      :user_id => 1
    ))
  end

  it "renders the edit forum_topic form" do
    render

    assert_select "form[action=?][method=?]", forum_topic_path(@forum_topic), "post" do

      assert_select "input#forum_topic_title[name=?]", "forum_topic[title]"

      assert_select "textarea#forum_topic_desc[name=?]", "forum_topic[desc]"

      assert_select "input#forum_topic_is_published[name=?]", "forum_topic[is_published]"

      assert_select "input#forum_topic_user_id[name=?]", "forum_topic[user_id]"
    end
  end
end
