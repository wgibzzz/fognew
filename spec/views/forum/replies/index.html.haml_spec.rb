require 'rails_helper'

RSpec.describe "forum/replies/index", :type => :view do
  before(:each) do
    assign(:forum_replies, [
      Forum::Reply.create!(
        :title => "Title",
        :body => "MyText",
        :is_published => false,
        :user_id => 1
      ),
      Forum::Reply.create!(
        :title => "Title",
        :body => "MyText",
        :is_published => false,
        :user_id => 1
      )
    ])
  end

  it "renders a list of forum/replies" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
  end
end
