require 'rails_helper'

RSpec.describe "forum/replies/edit", :type => :view do
  before(:each) do
    @forum_reply = assign(:forum_reply, Forum::Reply.create!(
      :title => "MyString",
      :body => "MyText",
      :is_published => false,
      :user_id => 1
    ))
  end

  it "renders the edit forum_reply form" do
    render

    assert_select "form[action=?][method=?]", forum_reply_path(@forum_reply), "post" do

      assert_select "input#forum_reply_title[name=?]", "forum_reply[title]"

      assert_select "textarea#forum_reply_body[name=?]", "forum_reply[body]"

      assert_select "input#forum_reply_is_published[name=?]", "forum_reply[is_published]"

      assert_select "input#forum_reply_user_id[name=?]", "forum_reply[user_id]"
    end
  end
end
