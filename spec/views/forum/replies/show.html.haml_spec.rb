require 'rails_helper'

RSpec.describe "forum/replies/show", :type => :view do
  before(:each) do
    @forum_reply = assign(:forum_reply, Forum::Reply.create!(
      :title => "Title",
      :body => "MyText",
      :is_published => false,
      :user_id => 1
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/1/)
  end
end
