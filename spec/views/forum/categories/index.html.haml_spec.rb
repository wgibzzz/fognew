require 'rails_helper'

RSpec.describe "forum/categories/index", :type => :view do
  before(:each) do
    assign(:forum_categories, [
      Forum::Category.create!(
        :title => "Title",
        :desc => "MyText",
        :is_published => false
      ),
      Forum::Category.create!(
        :title => "Title",
        :desc => "MyText",
        :is_published => false
      )
    ])
  end

  it "renders a list of forum/categories" do
    render
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
