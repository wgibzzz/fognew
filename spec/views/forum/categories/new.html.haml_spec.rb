require 'rails_helper'

RSpec.describe "forum/categories/new", :type => :view do
  before(:each) do
    assign(:forum_category, Forum::Category.new(
      :title => "MyString",
      :desc => "MyText",
      :is_published => false
    ))
  end

  it "renders new forum_category form" do
    render

    assert_select "form[action=?][method=?]", forum_categories_path, "post" do

      assert_select "input#forum_category_title[name=?]", "forum_category[title]"

      assert_select "textarea#forum_category_desc[name=?]", "forum_category[desc]"

      assert_select "input#forum_category_is_published[name=?]", "forum_category[is_published]"
    end
  end
end
