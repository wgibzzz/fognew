require 'rails_helper'

RSpec.describe "forum/categories/show", :type => :view do
  before(:each) do
    @forum_category = assign(:forum_category, Forum::Category.create!(
      :title => "Title",
      :desc => "MyText",
      :is_published => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/MyText/)
    expect(rendered).to match(/false/)
  end
end
