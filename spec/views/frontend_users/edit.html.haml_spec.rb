require 'rails_helper'

RSpec.describe "frontend_users/edit", :type => :view do
  before(:each) do
    @frontend_user = assign(:frontend_user, FrontendUser.create!())
  end

  it "renders the edit frontend_user form" do
    render

    assert_select "form[action=?][method=?]", frontend_user_path(@frontend_user), "post" do
    end
  end
end
