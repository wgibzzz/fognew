require 'rails_helper'

RSpec.describe "frontend_users/new", :type => :view do
  before(:each) do
    assign(:frontend_user, FrontendUser.new())
  end

  it "renders new frontend_user form" do
    render

    assert_select "form[action=?][method=?]", frontend_users_path, "post" do
    end
  end
end
