require 'spec_helper'

describe User do
  it ' - should not create without password' do
    user = User.create!(email: 'test@spec.rb')
    expect(user).to_not be_valid
  end
end
