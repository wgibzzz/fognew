class CategoriesController < InheritedResources::Base
  def index
    @page_title = I18n.t('fog.fe.layout.nav.categories')
    @categories = Category.all.frontend_page params[:page]
  end

  def show
    @category = Category.find_by_slug(params[:id])
    @page_title = "#{I18n.t('fog.fe.layout.nav.categories')} | #{@category.title}"
    @posts = @category.posts.frontend_page params[:page]
  end

  private

    def category_params
      params.require(:category).permit(:title, :desc, :parent_id, :user_id)
    end
end

