class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  helper_method :total_online, :anons_online, :count_online
  after_action :set_online
  layout :layout_by_resource

  def set_forum_collapse
    if user_signed_in?
      @user = User.find(current_user.id)
      settings = JSON.decode(@user.settings)
      if params[:action] == 'collapse'
        settings['forum']['cat_collapse'] << params[:id]
      elsif params[:action] == 'show'
        settings['forum']['cat_collapse'].delete(params[:id])
      end
      @user.update_attribute(:settings, settings)
    else
    end
    respond_to do |format|
      if @user.nil?
        format.json { render json: {}, status: :ok }
      else
        format.json { render json: @user.settings, status: :ok }
      end
    end
  end

  private

  def set_online
    if !!current_user
      # вошедшему пользователю к ключу добавляем префикс "user:" перед id
      $redis_online.set("user:#{current_user.id}", nil, ex: 10*60)
    else
      # не вошедшему пользователю добавляем префикс "ip:" и записываем его id адрес
      $redis_online.set("ip:#{request.remote_ip}", nil, ex: 10*60)
    end
  end

  # все вошедшие пользователи онлайн (массив с их id)
  def total_online
    ids = []
    $redis_online.scan_each(match: 'user*') { |u| ids << u.gsub("user:", "") }
    @users = []
    ids.each { |id| @users << User.find(id).decorate }
    @users
  end

  def users_online
    $redis_online.scan_each(match: 'user*').to_a.size
  end

  def anons_online
    $redis_online.scan_each(match: 'ip*').to_a.size
  end

  # количество всех пользователей онлайн
  def count_online
    $redis_online.dbsize
  end

  def count_online_html
    anon_qty = anons_online if anons_online > 0
    user_qty = users_online if users_online > 0
    str = "#{t('fog.fe.layout.block.whois_online.total_qty')+count_online}#{
    unless user_qty.nil?
      t('fog.fe.layout.block.whois_online.user_qty')
    end}"
  end

  protected

  def layout_by_resource
    if devise_controller?
      'mini'
    else
      'application'
    end
  end
end
