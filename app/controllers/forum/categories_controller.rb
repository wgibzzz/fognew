class Forum::CategoriesController < ApplicationController
  before_action :set_forum_category, only: [:show, :edit, :update, :destroy]

  # GET /forum/categories
  # GET /forum/categories.json
  def index
    @page_title = I18n.t('fog.fe.layout.nav.forum')
    @forum_categories = Forum::Category.includes(children: [children: :children]).where(:parent_id => nil)
  end

  # GET /forum/categories/1
  # GET /forum/categories/1.json
  def show
    @page_title = "#{I18n.t('fog.fe.layout.nav.forum')} | #{@forum_category.title}"
    @topics = Forum::Topic.where(:category_id => @forum_category.id).frontend_page params[:page]
  end

  # GET /forum/categories/new
  def new
    @forum_category = Forum::Category.new
    @mapped_forum_categories = Forum::Category.walk_tree { |category, level| puts "#{'-'*level}#{category.title}"  }
  end

  # GET /forum/categories/1/edit
  def edit
  end

  # POST /forum/categories
  # POST /forum/categories.json
  def create
    @forum_category = Forum::Category.new(forum_category_params)

    respond_to do |format|
      if @forum_category.save
        format.html { redirect_to @forum_category, notice: 'Category was successfully created.' }
        format.json { render :show, status: :created, location: @forum_category }
      else
        format.html { render :new }
        format.json { render json: @forum_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /forum/categories/1
  # PATCH/PUT /forum/categories/1.json
  def update
    respond_to do |format|
      if @forum_category.update(forum_category_params)
        format.html { redirect_to @forum_category, notice: 'Category was successfully updated.' }
        format.json { render :show, status: :ok, location: @forum_category }
      else
        format.html { render :edit }
        format.json { render json: @forum_category.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /forum/categories/1
  # DELETE /forum/categories/1.json
  def destroy
    @forum_category.destroy
    respond_to do |format|
      format.html { redirect_to forum_categories_url, notice: 'Category was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_forum_category
      @forum_category = Forum::Category.includes(:parent).find_by_slug(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def forum_category_params
      params.require(:forum_category).permit(:title, :desc, :user_id, :parent_id, :is_published)
    end
end
