class Forum::TopicsController < ApplicationController
  before_action :set_forum_topic, only: [:show, :new, :edit, :update, :destroy]
  before_action :set_forum_category, only: [:new]

  # GET /forum/topics
  # GET /forum/topics.json
  def index
    @forum_topics = Forum::Topic.includes(:replies, :user).all
  end

  # GET /forum/topics/1
  # GET /forum/topics/1.json
  def show
    @page_title = "#{I18n.t('fog.fe.layout.nav.forum')} | #{@forum_topic.category.title} | #{@forum_topic.title}"
    @replies = Forum::Reply.includes(:user).where(:topic_id => @forum_topic.id).frontend_page params[:page]
  end

  # GET /forum/topics/new
  def new
    #@forum_topic = @forum_category.topics.new
    @forum_topic = Forum::Topic.new({:category_id => Forum::Category.find_by_slug(params[:category_id]).id})
    @forum_topic.replies.build
  end

  # GET /forum/topics/1/edit
  def edit
  end

  # POST /forum/topics
  # POST /forum/topics.json
  def create
    @forum_topic = Forum::Topic.new(forum_topic_params)
    respond_to do |format|
      if @forum_topic.save
        format.html { redirect_to forum_category_topic_path(@forum_topic.category, @forum_topic), notice: 'Topic was successfully created.' }
        format.json { render :show, status: :created, location: @forum_topic }
      else
        format.html { render :new }
        format.json { render json: @forum_topic.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /forum/topics/1
  # PATCH/PUT /forum/topics/1.json
  def update
    respond_to do |format|
      if @forum_topic.update(forum_topic_params)
        format.html { redirect_to @forum_topic, notice: 'Topic was successfully updated.' }
        format.json { render :show, status: :ok, location: @forum_topic }
      else
        format.html { render :edit }
        format.json { render json: @forum_topic.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /forum/topics/1
  # DELETE /forum/topics/1.json
  def destroy
    @forum_topic.destroy
    respond_to do |format|
      format.html { redirect_to forum_topics_url, notice: 'Topic was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  def set_forum_category
    @forum_category = Forum::Category.find_by_slug(params[:category_id])
  end

  # Use callbacks to share common setup or constraints between actions.
  def set_forum_topic
    @forum_topic = Forum::Topic.includes(:category).find_by_slug(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def forum_topic_params
    params.require(:forum_topic).permit(:title, :category_id, :user_id, replies_attributes: [:user_id, :title, :body])
  end
end
