class Forum::RepliesController < InheritedResources::Base

  private

    def reply_params
      params.require(:reply).permit(:title, :body, :is_published, :user_id, :topic_id)
    end
end