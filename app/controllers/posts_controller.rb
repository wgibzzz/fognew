class PostsController < InheritedResources::Base
  def index
    @page_title = I18n.t('fog.fe.layout.nav.home')
    @posts = Post.includes(:category, :user).order(:created_at => :desc).frontend_page params[:page]
  end

  def show
    @post = Post.includes(:user, :category).find_by_slug(params[:id])
    @page_title = "#{@post.category.title} | #{@post.title}"
    @comments = Comment.includes(:user).where(:post_id => @post.id).frontend_page params[:comments_page]
    respond_to do |format|
      format.js
      format.json
      format.html
    end
  end

  private

    def post_params
      params.require(:post).permit(:title, :slug, :body, :user_id, :is_published, :comment)
    end
end

