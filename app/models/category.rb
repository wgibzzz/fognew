class Category < ActiveRecord::Base
  extend ActsAsTree::TreeWalker
  extend ActsAsTree::TreeView
  include PublicActivity::Model
  tracked

  acts_as_tree order 'title'
  acts_as_url :title, :url_attribute => :slug
  has_many :posts
  belongs_to :user

  def to_param
    slug
  end

end
