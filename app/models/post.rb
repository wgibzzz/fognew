class Post < ActiveRecord::Base
  include PublicActivity::Model
  tracked

  validates_presence_of :title, :body, :user, :category

  belongs_to :user
  belongs_to :category
  has_many :comments
  accepts_nested_attributes_for :comments
  acts_as_url :title, :url_attribute => :slug
  def to_param
    slug
  end
end
