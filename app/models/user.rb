class User < ActiveRecord::Base
  include PublicActivity::Model
  tracked
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_and_belongs_to_many :roles
  has_many :posts
  has_attached_file :avatar, :styles => { :big => '600x600>', :medium => '300x300>', :thumb => '100x100>'}, :default_url => '/images/:style/missing.png'
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

  #acts_as_url :name, :url_attribute => :slug

  def online?
    $redis_online.exists( "user:#{self.id}" )
  end

  def send_devise_notification(notification, *args)
    devise_mailer.send(notification, self, *args).deliver_later
  end

  # def to_param
  #   slug
  # end
end
