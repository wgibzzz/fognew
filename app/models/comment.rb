class Comment < ActiveRecord::Base
  include PublicActivity::Model
  tracked
  belongs_to :user
  belongs_to :post
  validates_presence_of :body
end
