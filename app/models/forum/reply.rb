class Forum::Reply < ActiveRecord::Base
  include PublicActivity::Model
  after_create :update_parent_category
  tracked

  belongs_to :user
  #belongs_to :category, :class_name => 'Forum::Category', :inverse_of => :last_post
  belongs_to :topic, :class_name => 'Forum::Topic', :foreign_key => :topic_id

  default_scope { order(:created_at => :asc) }

  private
  def update_parent_category
    last_post = self
    topic = Forum::Topic.find(self.topic_id)
    cat = Forum::Category.find(topic.category_id)
    cat.self_and_ancestors.each do |entry|
      entry.reply_count += 1
      entry.last_post_id = last_post.id
      entry.save
    end
  end
end
