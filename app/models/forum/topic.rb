class Forum::Topic < ActiveRecord::Base
  include PublicActivity::Model
  tracked

  acts_as_url :title, :url_attribute => :slug
  default_scope { order(:updated_at => :desc) }

  belongs_to :user
  belongs_to :category, :class_name => 'Forum::Category'
  has_many :replies, :class_name => 'Forum::Reply'
  accepts_nested_attributes_for :replies

  validates_presence_of :title, :category, :user

  after_create :update_parent_category

  def to_param
    slug
  end

  private
  def update_parent_category
    Forum::Category.find(self.category.self_and_ancestors.map(&:id)).each do |cat|
      cat.topic_count += 1
      cat.save
    end
  end

end
