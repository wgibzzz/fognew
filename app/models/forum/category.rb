class Forum::Category < ActiveRecord::Base
  include PublicActivity::Model
  tracked
  extend ActsAsTree::TreeView
  extend ActsAsTree::TreeWalker
  belongs_to :user
  has_many :topics, :class_name => 'Forum::Topic'
  #has_one :last_post, :class_name => 'Forum::Reply'

  acts_as_tree order: 'title'
  acts_as_url :title, :url_attribute => :slug
  default_scope { order(:ordering => :asc) }
  def to_param
    slug
  end

  # def topic_count
  #   Forum::Topic.where(:category_id => self.self_and_descendants.map(&:id)).count
  #   #Forum::Topic.eager_load(:category).where(:category => self.self_and_descendants).count
  # end
  # def reply_count
  #   #Forum::Reply.where(:topic_id => Forum::Topic.where(:category_id => self.self_and_descendants.map(&:id)).pluck(:id)).count
  #   Forum::Reply.eager_load(:topic).where(:topic => Forum::Topic.eager_load(:category).where(:category => self.self_and_descendants)).count
  # end
  def last_post
    Forum::Reply.includes(:user, :topic).find(self.last_post_id)
  end
  # def last_post
  #   Forum::Reply.where(:topic_id => Forum::Topic.where(:category_id => self.self_and_descendants.map(&:id)).pluck(:id)).order(:created_at => :desc).first
  #   #Forum::Reply.eager_load(:topic).where(:topic => Forum::Topic.eager_load(:category).where(:category => self.self_and_descendants)).order(:created_at => :desc).first
  # end
end
