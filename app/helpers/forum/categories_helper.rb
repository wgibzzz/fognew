module Forum::CategoriesHelper
  def form_parent_select
    @nested_values = []
    Forum::Category.walk_tree { |category, level| @nested_values.push(["#{'-'*level}#{category.title}", category.id]) }
    @nested_values
  end
  def topic_count(category)
    unless category.nil?
      count = category.topics.count
      if category.children.any?
        category.children.each do |children|
          count += children.decorate.topic_count
        end
      end
      count
    end
  end
  def post_count(category)
    unless category.nil?
      count = category.topics.count
      if category.children.any?
        category.children.each do |children|
          count += children.decorate.topic_count
        end
      end
      count
    end
  end
end