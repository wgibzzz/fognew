class UserDecorator < ApplicationDecorator
  delegate_all
  decorates_finders

  # Define presentation-specific methods here. Helpers are accessed through
  # `helpers` (aka `h`). You can override attributes, for example:
  #
  #   def created_at
  #     helpers.content_tag :span, class: 'time' do
  #       object.created_at.strftime("%a %m/%d/%y")
  #     end
  #   end

  def processed_avatar(size: 50, format: '.png', style: 'retro', attach_size: :thumb)
    unless object.nil?
      if object.avatar.exists?
        object.avatar.url(attach_size)
      else
        # create the md5 hash
        hash = Digest::MD5.hexdigest(object.email.downcase)

        # compile URL which can be used in <img src="RIGHT_HERE"...
        "http://www.gravatar.com/avatar/#{hash}#{format}?s=#{size}&d=#{style}"
      end

    end
  end

  def processed_name
    unless object.nil?
      if object.name.nil?
        object.email.downcase
      else
        object.name
      end
    end
  end

end
