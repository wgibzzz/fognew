class UserMigrateWorker
  include Sidekiq::Worker

  # @param [User] user
  # @return [String]
  def perform(user)
    puts user.name + ' migration started'
    @user = user
    @user.save
    puts 'User migrated. Password is: ' + user.password unless @user.errors.any?
  end
end