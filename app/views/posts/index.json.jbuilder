json.array!(@posts) do |post|
  json.extract! post, :id, :title, :slug, :body, :user_id, :is_published
  json.url post_url(post, format: :json)
end
