json.array!(@forum_categories) do |forum_category|
  json.extract! forum_category, :id, :title, :desc, :is_published
  json.url forum_category_url(forum_category, format: :json)
end
