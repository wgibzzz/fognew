# This is a manifest file that'll be compiled into application.js, which will include all the files
# listed below.
#
# Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
# or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
#
# It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
# compiled file.
#
# Read Sprockets README (https:#github.com/sstephenson/sprockets#sprockets-directives) for details
# about supported directives.
#= require taurus/plugins/jquery/jquery.min
#= require jquery_ujs
#= require taurus/plugins/jquery/jquery-ui.min
#= require taurus/plugins/jquery/jquery-migrate.min
#= require taurus/plugins/jquery/globalize
#= require taurus/plugins/bootstrap/bootstrap.min
#= require taurus/plugins/fancybox/jquery.fancybox.pack
#= require taurus/plugins/noty/jquery.noty
#= require taurus/plugins/noty/layouts/topRight
#= require taurus/plugins/noty/themes/default
#= require tinymce-jquery
#= require taurus/plugins
#= require taurus/actions
$(window).on 'scroll resize load', (e) ->

  if Math.floor($(this).scrollTop()) <= 750
    distance = Math.floor($(this).scrollTop() / 200)
  else
    distance = 3
  $('.bg-blur').css {
    '-webkit-filter': "blur(#{distance}px)",
    'filter': "blur(#{distance}px)"
  }
  return