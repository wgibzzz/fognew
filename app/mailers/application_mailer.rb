class ApplicationMailer < ActionMailer::Base
  default from: 'noreply@fog-ps2.ru'
  layout 'mailer'
end
