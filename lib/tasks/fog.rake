namespace :fog do
  namespace :forum do
    desc 'Update topic and reply counters in categories'
    task update_counters: :environment do
      PublicActivity.without_tracking do
        Forum::Topic.unscoped.all.order(:id => :asc).each do |topic|
          topic.reply_count = topic.replies.count
          topic.save
        end
        Forum::Category.unscoped.all.order(:id => :asc).each do |cat|
          cat.topic_count = Forum::Topic.where(:category_id => cat.self_and_children.map(&:id)).count
          cat.reply_count = Forum::Topic.where(:category_id => cat.self_and_children.map(&:id)).pluck(:reply_count).inject(0) { |result, val| result = val }
          cat.save
        end
      end
    end
    desc 'Update reply-category relations'
    task update_replies_with_catid: :environment do
      PublicActivity.without_tracking do
        Forum::Category.all.each do |cat|
          last_post = Forum::Reply.joins(:topic).order(:created_at => :asc).where(forum_topics: {category_id: cat.self_and_children.map(&:id)}).last
          puts cat.title
          unless last_post.nil?
            cat.last_post_id = last_post.id
            unless cat.root?
              parent = Forum::Category.find(cat.parent.id)
              if parent.last_post_id.nil?
                parent.last_post_id = last_post.id
              elsif !parent.last_post_id.nil? and Time.at(parent.last_post.created_at) < Time.at(last_post.created_at)
                parent.last_post_id = last_post.id
              end
              parent.save
            end
            cat.save
          end
        end
      end
    end
  end
end
