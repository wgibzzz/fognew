require 'mysql2'
require 'date'

config = {
    host: 'localhost',
    username: 'root',
    password: '222222',
    db_old: 'site_fog',
    db_new: 'site_fog_dev_migration',
}

namespace :joomla do
  namespace :users do
    desc 'Migrate users (Basic fields)'
    task migrate_basic: :environment do
      old_db = Mysql2::Client.new(:host => config[:host], :username => config[:username], :password => config[:password], :database => config[:db_old])
      users = []
      old_db.query('
      SELECT u.id, u.name, u.email, u.registerDate
      FROM site_fog.fog_users AS u
      LIMIT 100000;').each do |row|
        users << row
      end

      puts 'User count: ' + users.count.to_s
      #puts 'Row example:'
      #puts users.first
      puts 'Now we starting users migration'
      File.truncate(File.expand_path('../../../log/users.log', __FILE__), 0)
      File.open(File.expand_path('../../../log/users.log', __FILE__), 'a') do |file|
        ActiveRecord::Base.record_timestamps = false
        begin
          file.puts 'user:email:password'
          users.each do |dbUser|
            #puts dbUser
            pass = Faker::Internet::password(8, 15)
            if User.find_by_migrator_id(dbUser['id']).nil?
              user = User.new
            else
              user = User.find_by_migrator_id(dbUser['id'])
            end
            user.name = dbUser['name']
            user.email = dbUser['email']
            user.password = pass
            user.password_confirmation = pass
            user.migrator_id = dbUser['id']
            user.created_at = dbUser['registerDate']
            user.save
            file.puts "#{dbUser['name']}:#{dbUser['email']}:#{pass}"
          end
        ensure
          ActiveRecord::Base.record_timestamps = true
        end
      end
    end
    desc 'Migrate users (Advanced fields)'
    task migrate_advanced: :environment do
      old_db = Mysql2::Client.new(:host => config[:host], :username => config[:username], :password => config[:password], :database => config[:db_old])
      # Select advanced fields
      old_db.query('
        SELECT userid, signature, avatar, gender, birthdate
        FROM site_fog.fog_kunena_users
        LIMIT 100000;
      ', :cast => false, :symbolize_keys => true).each do |raw|

        # And check results. We expect, that some fields should be non legit
        processed = raw
        processed[:birthdate] = nil if raw[:birthdate] == '0001-01-01'
        processed[:birthdate] = nil if raw[:birthdate] == '0000-00-00'
        processed[:avatar] == nil if raw[:avatar].to_s == ''
        processed[:signature] == nil if raw[:signature].to_s == ''
        if raw[:gender].to_i == 0
          processed[:gender] = nil
        else
          processed[:gender] = raw[:gender].to_i - 1
        end

        # Then we find user by old id
        user = User.find_by_migrator_id(processed[:userid])
        unless user.nil?
          # Birth date
          begin
            unless user.birth_date.nil?
              user.birth_date = Date.parse(processed[:birthdate]) unless processed[:birthdate].nil?
            end
          rescue
            puts "Wrong date #{processed[:birthdate]} :( \n"
          end

          # Signature
          unless user.signature.nil?
            user.signature = processed[:signature] unless processed[:signature].nil?
          end

          # Avatar
          unless processed[:avatar].nil?
            unless user.avatar.exists?
              attach_name = 'avatar'
              attach = Paperclip::Attachment.new(attach_name, user, user.class.attachment_definitions[attach_name.to_sym])
              begin
                avatar = File.open(File.expand_path(('../../../migration/'+processed[:avatar]).to_s, __FILE__))
                attach.assign avatar
                attach.save
                avatar.close
              rescue
                puts "File not found :( \n"
              end
            end
          end

          # Gender
          if user.gender.nil?
            user.gender = processed[:gender] unless processed[:gender].nil?
          end

          user.save
        end
        #puts processed
      end
    end
  end
  namespace :categories do
    desc 'Migrate post categories'
    task migrate: :environment do
      old_db = Mysql2::Client.new(:host => config[:host], :username => config[:username], :password => config[:password], :database => config[:db_old])
      # Selecting rows
      old_db.query('
        SELECT id, parent_id, title, created_user_id, description
        FROM site_fog.fog_categories
        WHERE extension = \'com_content\' AND id != 10 AND id != 14
        ORDER BY id ASC;
      ', :cast => false, :symbolize_keys => true).each do |row|
        #puts row

        # Instantiating new object
        if Category.find_by_migrator_id(row[:id]).nil?
          post_category = Category.new
        else
          post_category = Category.find_by_migrator_id(row[:id].to_i)
        end

        # Setting title
        post_category.title = row[:title]

        # Setting description
        post_category.desc = row[:description] unless row[:description] == ''


        # Setting migration_id
        post_category.migrator_id = row[:id].to_i

        # Setting parent, if parent_id > 1
        post_category.parent_id = Category.find_by_migrator_id(row[:parent_id].to_i).id if row[:parent_id].to_i > 1

        # Setting user id if created_user_id != 0
        post_category.user_id = User.find_by_migrator_id(row[:created_user_id].to_i).id if row[:created_user_id].to_i > 0

        post_category.save
        puts "Category #{post_category.id} migrated"
      end
    end
  end
  namespace :posts do
    desc 'Migrate posts (basic)'
    task migrate_basic: :environment do
      old_db = Mysql2::Client.new(:host => config[:host], :username => config[:username], :password => config[:password], :database => config[:db_old])
      old_db.query('
        SELECT id, title, introtext, `fulltext`, created_by, catid, created, modified
        FROM site_fog.fog_content
        WHERE site_fog.fog_content.catid = 2
        OR site_fog.fog_content.catid = 8 OR site_fog.fog_content.catid = 9 OR site_fog.fog_content.catid = 15
        ORDER BY site_fog.fog_content.modified DESC;
      ', :symbolize_keys => true).each do |row|
        #puts row
        ActiveRecord::Base.record_timestamps = false
        begin
          if Post.find_by_migrator_id(row[:id]).nil?
            post = Post.new
          else
            post = Post.find_by_migrator_id(row[:id])
          end
          if post.title.nil?
            post.title = row[:title]
          end
          if post.user.nil?
            post.user = User.find_by_migrator_id(row[:created_by])
          end
          if post.category.nil?
            post.category = Category.find_by_migrator_id(row[:catid])
          end
          if post.body.nil?
            post.body = (row[:introtext] + '<!--readmore-->' + row[:fulltext]).to_s
          end
          post.migrator_id = row[:id]
          post.created_at = row[:created]
          post.updated_at = row[:modified] unless row[:modified].nil?
          post.save
          puts post.errors.keys
          begin
            post.save
            puts "Post #{post.id} has been migrated"
          rescue ArgumentError
            puts post.errors
          end

        ensure
          ActiveRecord::Base.record_timestamps = true
        end
      end
    end
  end
  namespace :post_comments do
    desc 'Migrate post comments'
    task migrate_comments: :environment do
      old_db = Mysql2::Client.new(:host => config[:host], :username => config[:username], :password => config[:password], :database => config[:db_old])
      old_db.query('
        SELECT r.*, m.thread, m.id, m.parent, m.userid, m.time, m.modified_time, t.* FROM site_fog.fog_kunena_messages AS m
          RIGHT JOIN site_fog.fog_kunenadiscuss AS r
            ON m.thread = r.thread_id
          LEFT OUTER JOIN site_fog.fog_kunena_messages_text AS t
            ON t.mesid = m.id
          WHERE m.parent != 0
          ORDER BY m.time ASC
        LIMIT 100000;
      ', :symbolize_keys => true).each do |row|
        processed = row
        ActiveRecord::Base.record_timestamps = false
        begin
          if Comment.find_by_migrator_id(processed[:id].to_i).nil?
            comment = Comment.new
          else
            comment = Comment.find_by_migrator_id(processed[:id].to_i)
          end
          comment.migrator_id = processed[:id].to_i
          comment.user = User.find_by_migrator_id(processed[:userid].to_i)
          comment.post = Post.find_by_migrator_id(processed[:content_id].to_i)
          comment.body = processed[:message]
          comment.created_at = Time.at(processed[:time])
          comment.updated_at = Time.at(processed[:modified_time]) unless processed[:modified_time].nil?
          comment.save
          #puts comment.errors.messages if comment.errors.any?
          #puts processed
        ensure
          ActiveRecord::Base.record_timestamps = true
        end
      end
      # SELECT * FROM site_fog.fog_udjacomments LIMIT 100000;
    end
  end
  namespace :forum do
    desc 'Migrate forum categories'
    task migrate_categories: :environment do
      old_db = Mysql2::Client.new(:host => config[:host], :username => config[:username], :password => config[:password], :database => config[:db_old])
      old_db.query('
        SELECT id, parent_id, name, ordering, numTopics, numPosts, last_post_id
        FROM site_fog.fog_kunena_categories
        ORDER BY parent_id ASC, id ASC;
      ', :symbolize_keys => true).each do |row|
        #puts row
        if Forum::Category.find_by_migrator_id(row[:id]).nil?
          forum_category = Forum::Category.new
        else
          forum_category = Forum::Category.find_by_migrator_id(row[:id].to_i)
          forum_category.last_post_id = Forum::Reply.includes()
        end
        forum_category.title = row[:name]
        forum_category.user_id = 1
        forum_category.migrator_id = row[:id].to_i
        forum_category.ordering = row[:ordering].to_i
        forum_category.topic_count = row[:numTopics].to_i
        forum_category.reply_count = row[:numPosts].to_i

        if row[:parent_id] != 0
          forum_category.parent = Forum::Category.find_by_migrator_id(row[:parent_id].to_i)
        end
        forum_category.save
      end
    end
    desc 'Migrate forum topics'
    task migrate_topics: :environment do
      old_db = Mysql2::Client.new(:host => config[:host], :username => config[:username], :password => config[:password], :database => config[:db_old])
      old_db.query('
        SELECT id, category_id, subject, first_post_userid, first_post_time, posts
        FROM site_fog.fog_kunena_topics
        ORDER BY category_id ASC;
      ', :symbolize_keys => true).each do |row|
        #puts row
        ActiveRecord::Base.record_timestamps = false
        begin
          if Forum::Topic.find_by_migrator_id(row[:id]).nil?
            forum_topic = Forum::Topic.new
          else
            forum_topic = Forum::Topic.find_by_migrator_id(row[:id].to_i)
          end
          forum_topic.title = row[:subject]
          forum_topic.migrator_id = row[:id].to_i
          forum_topic.reply_count = row[:posts].to_i
          forum_topic.user = User.find_by_migrator_id(row[:first_post_userid].to_i)
          forum_topic.category = Forum::Category.find_by_migrator_id(row[:category_id].to_i)
          forum_topic.created_at = Time.at(row[:first_post_time])
          forum_topic.save
        ensure
          ActiveRecord::Base.record_timestamps = true
        end
      end
    end
    desc 'Migrate posts'
    task migrate_posts: :environment do
      old_db = Mysql2::Client.new(:host => config[:host], :username => config[:username], :password => config[:password], :database => config[:db_old])
      old_db.query('
        SELECT m.*, t.message
        FROM site_fog.fog_kunena_messages AS m
        LEFT JOIN site_fog.fog_kunena_messages_text AS t
        ON m.id = t.mesid
        ORDER BY id ASC, time ASC
        LIMIT 900000;
      ', :symbolize_keys => true).each do |row|
        #puts row
        ActiveRecord::Base.record_timestamps = false
        begin
          if Forum::Reply.find_by_migrator_id(row[:id]).nil?
            forum_reply = Forum::Reply.new
          else
            forum_reply = Forum::Reply.find_by_migrator_id(row[:id].to_i)
          end
          forum_reply.migrator_id = row[:id].to_i
          forum_reply.user = User.find_by_migrator_id(row[:userid].to_i)
          forum_reply.topic = Forum::Topic.find_by_migrator_id(row[:thread].to_i)
          #forum_reply.category_id = Forum::Category.find_by_migrator_id(row[:catid].to_i).id
          forum_reply.title = row[:subject]
          forum_reply.body = row[:message]
          forum_reply.created_at = Time.at(row[:time])
          forum_reply.updated_at = Time.at(row[:modified_time]) unless row[:modified_time].nil?
          forum_reply.save
        ensure
          ActiveRecord::Base.record_timestamps = true
        end
      end
    end
    desc 'Update relations for forum categories'
    task update_category_relations: :environment do

    end
  end
end