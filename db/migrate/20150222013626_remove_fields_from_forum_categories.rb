class RemoveFieldsFromForumCategories < ActiveRecord::Migration
  def change
    remove_column :forum_categories, :topic_count, :integer
    remove_column :forum_categories, :post_count, :integer
  end
end
