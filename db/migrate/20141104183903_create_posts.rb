class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.string :slug
      t.text :body
      t.integer :user_id
      t.boolean :is_published

      t.integer :migrator_id

      t.timestamps
    end
  end
end
