class AddCategoryIdToForumTopic < ActiveRecord::Migration
  def change
    add_column :forum_topics, :category_id, :integer
  end
end
