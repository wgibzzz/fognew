class AddForumTopicIdToForumReply < ActiveRecord::Migration
  def change
    add_column :forum_replies, :topic_id, :integer
  end
end
