class CreateForumCategories < ActiveRecord::Migration
  def change
    create_table :forum_categories do |t|
      t.string :title
      t.text :desc
      t.boolean :is_published

      t.integer :migrator_id

      t.timestamps
    end
  end
end
