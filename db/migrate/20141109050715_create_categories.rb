class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :title
      t.text :desc
      t.integer :parent_id
      t.integer :user_id

      t.integer :migrator_id

      t.timestamps
    end
  end
end
