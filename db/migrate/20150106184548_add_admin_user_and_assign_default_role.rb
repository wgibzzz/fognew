class AddAdminUserAndAssignDefaultRole < ActiveRecord::Migration
  def self.up
    user = User.new(email: 'root@fog-ps2.ru', password: 'Squad1995', password_confirmation: 'Squad1995')
    user.roles << Role.find_by(slug: 'admin')
    user.save
    unless user.errors.any?
      puts 'Root user created. Yay!'
    end
  end

  def self.down
    User.find_by_email('root@fog-ps2.ru').destroy!
    puts 'Root user destroyed'
  end
end