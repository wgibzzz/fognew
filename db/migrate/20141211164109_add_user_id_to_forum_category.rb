class AddUserIdToForumCategory < ActiveRecord::Migration
  def change
    add_column :forum_categories, :user_id, :integer
  end
end
