class CreateForumTopics < ActiveRecord::Migration
  def change
    create_table :forum_topics do |t|
      t.string :title
      t.text :desc
      t.boolean :is_published
      t.integer :user_id

      t.integer :migrator_id

      t.timestamps
    end
  end
end
