class AddFieldsToForumCategories < ActiveRecord::Migration
  def change
    add_column :forum_categories, :reply_count, :integer
    add_column :forum_categories, :topic_count, :integer
    add_column :forum_categories, :last_post_id, :integer
  end
end
