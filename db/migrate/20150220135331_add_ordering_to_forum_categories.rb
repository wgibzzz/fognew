class AddOrderingToForumCategories < ActiveRecord::Migration
  def change
    add_column :forum_categories, :ordering, :integer
  end
end
