class AddFieldsToForumTopic < ActiveRecord::Migration
  def change
    add_column :forum_topics, :reply_count, :integer
  end
end
