class AddParentIdToForumCategories < ActiveRecord::Migration
  def change
    add_column :forum_categories, :parent_id, :integer
  end
end
