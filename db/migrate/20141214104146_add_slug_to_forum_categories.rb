class AddSlugToForumCategories < ActiveRecord::Migration
  def change
    add_column :forum_categories, :slug, :string
  end
end
