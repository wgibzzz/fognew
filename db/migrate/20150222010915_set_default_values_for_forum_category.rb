class SetDefaultValuesForForumCategory < ActiveRecord::Migration
  def up
    Forum::Category.find_each do |cat|
      cat.update_attribute(:topic_count, 0)
      cat.update_attribute(:post_count, 0)
    end
  end
end
