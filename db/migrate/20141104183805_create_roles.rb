class CreateRoles < ActiveRecord::Migration
  def migrate(direction)
    super
    # Create a default user
    Role.create!(title: 'Admin', slug: 'admin') if direction == :up
    Role.create!(title: 'User', slug: 'user') if direction == :up
    Role.create!(title: 'Guest', slug: 'guest') if direction == :up
  end
  def change
    create_table :roles do |t|
      t.string :title
      t.string :slug

      t.integer :migrator_id

      t.timestamps
    end
  end
end
