class AddSlugToForumTopics < ActiveRecord::Migration
  def change
    add_column :forum_topics, :slug, :string
  end
end
