class CreateForumReplies < ActiveRecord::Migration
  def change
    create_table :forum_replies do |t|
      t.string :title
      t.text :body
      t.boolean :is_published
      t.integer :user_id

      t.integer :migrator_id

      t.timestamps
    end
  end
end
