class AddMigrationIdToComments < ActiveRecord::Migration
  def change
    add_column :comments, :migrator_id, :integer
  end
end
