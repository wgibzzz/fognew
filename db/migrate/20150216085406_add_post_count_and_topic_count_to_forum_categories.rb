class AddPostCountAndTopicCountToForumCategories < ActiveRecord::Migration
  def change
    add_column :forum_categories, :post_count, :integer
    add_column :forum_categories, :topic_count, :integer
  end
end
