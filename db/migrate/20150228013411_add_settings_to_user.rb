class AddSettingsToUser < ActiveRecord::Migration
  enable_extension :hstore unless extension_enabled?(:hstore)
  def change
    add_column :users, :settings, :hstore
  end
end
