# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

10.times do
  title = Faker::Lorem.sentence
  slug = title.to_url
  child_category = Category.scoped.select(:id)
  cat = Category.new(
      title: Faker::Lorem.sentence,
      desc: Faker::Lorem.paragraph(1),
      user: User.first,
      slug: slug,
  )
  cat.children << Category.find(child_category.first(Random.rand(child_category.length)).last)
  cat.save
end
200.times do
  title = Faker::Lorem.sentence
  slug = title.to_url
  cat = Category.scoped.select(:id)
  post = Post.new(
      title: Faker::Lorem.sentence,
      body: Faker::Lorem.paragraph(4),
      user: User.first,
      slug: slug
  )
  post.children << Category.find(cat.first(Random.rand(cat.length)).last)
  post.save
end